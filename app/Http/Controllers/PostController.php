<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function index()
    {

        $posts = Post::all();
        if (!$posts) {
            throw new HttpException(400, "Invalid data");
        }
        return response()->json(
            $posts,
            200
        );
        //return json_encode($posts);
    }


    public function show($id)
    {
        $post = Post::find($id);
        return json_encode($post);
//        return view('posts.show', compact(
//            'post'
//        ));
    }
}
